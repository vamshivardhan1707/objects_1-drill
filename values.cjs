function values(obj) {
    if (typeof(obj) === 'object') {
        let valuesArray = [];

        for (let prop in obj) {
            valuesArray.push(obj[prop]);
        };
    return valuesArray;
    } else {
        return {};
    };
};

module.exports = values;