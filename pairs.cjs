function pairs(obj) {
    if (typeof(obj) === 'object') {
        let pairArray = [];

        for (let prop in obj) {
            pairArray.push([prop, obj[prop]]);
        };
    return pairArray;
    } else {
        return {};
    };
};

module.exports = pairs;