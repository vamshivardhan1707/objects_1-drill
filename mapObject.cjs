function mapObject(obj,cb) {
    if (typeof(obj) === 'object') {
        let newObject = {};

        for (let prop in obj) {
            newObject[prop] = cb(obj[prop]);
        };
    return newObject;
    } else {
        return {};
    };
};

module.exports = mapObject;