function keys(obj) {
    if (typeof(obj) === 'object') {
        let keyArray = [];

        for (let prop in obj) {
            keyArray.push(prop);
        };
    return keyArray;
    } else {
        return {};
    };
};

module.exports = keys;