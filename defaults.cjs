function defaults(obj, defaultProps) {
    if (typeof(obj) === "object" && typeof(defaultProps) === "object") {
        for (let prop in defaultProps) {
            if (obj[prop]) {
                continue;
            } else {
                obj[prop] = defaultProps[prop];
            };
        };
    return obj;
    } else {
        return {};
    };
};

module.exports = defaults;