function invert(obj) {
    if (typeof(obj) === 'object') {
        let invertedObj = {};

        for (let prop in obj) {
            invertedObj[obj[prop]] = prop;
        };
    return invertedObj;
    } else {
        return {};
    };
};

module.exports = invert;